import sys
from flask import Flask, request
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

class Info(Resource):
    def get(self):
        return {'version': 3, 'method': 'GET', 'message': 'Running'} # Fetches first column that is Employee ID

class Version(Resource):
    def get(self):
        return {'version': sys.version, 'method': 'GET', 'message': 'Running'} # Fetches first column that is Employee ID

api.add_resource(Info, '/get_info')

api.add_resource(Version, '/get_version')

if __name__ == '__main__':
     app.run(host='0.0.0.0', port='5290')

