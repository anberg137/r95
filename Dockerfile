# https://computingforgeeks.com/how-to-install-python-3-on-centos/

FROM centos:7

RUN yum -y groupinstall "Development Tools" && \ 
    yum -y install openssl-devel bzip2-devel libffi-devel && \
    yum -y install wget && \
    wget https://www.python.org/ftp/python/3.8.12/Python-3.8.12.tgz && \
    tar xvf Python-3.8.12.tgz && \
    cd Python-3.8*/ && \
    ./configure --enable-optimizations && \
    make altinstall

RUN pip3.8 install flask flask_restful flask_jsonpify
COPY api.py /python_api/api.py
CMD ["python3.8", "/python_api/api.py"]
